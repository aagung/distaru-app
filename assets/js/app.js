
function blockPage() {
    $.blockUI({
        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left fa-2x"></i>&nbsp;Loading...</span>',
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.6,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: '10px 15px',
            color: '#fff',
            '-webkit-border-radius': 2,
            '-moz-border-radius': 2,
            backgroundColor: '#333'
        }
    });
}

numeral.language('id', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'ribu',
        million: 'juta',
        billion: 'milyar',
        trillion: 'triliun'
    },
    currency: {
        symbol: 'Rp.'
    }
});
numeral.language('id');
	
// Data Tables - Config
(function ($) {

    'use strict';

    $.fn.getDropBoxApi = function (urlStr, idStr) {
        return $.ajax({
            url: urlStr,
            type: 'GET',
            dataType: 'json',
            success: function (json) {

                $('#' + idStr).append($('<option>').text('').attr('value', ''));
                $.each(json.result, function (i, value) {
                    $('#' + idStr).append($('<option>').text(value.nama).attr('value', value.uid));
                });
            }
        });
    }

    // we overwrite initialize of all datatables here
    // because we want to use select2, give search input a bootstrap look
    // keep in mind if you overwrite this fnInitComplete somewhere,
    // you should run the code inside this function to keep functionality.
    //
    // there's no better way to do this at this time :(
    if ($.isFunction($.fn[ 'dataTable' ])) {
        $.extend(true, $.fn.dataTable.defaults, {
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Cari:</span> _INPUT_',
                lengthMenu: '<span>Jumlah per halaman:</span> _MENU_',
                processing: '<i class="fa fa-spinner fa-spin"></i> Loading',
                paginate: {'first': 'Awal', 'last': 'Akhir', 'next': '&rarr;', 'previous': '&larr;'},
                emptyTable: "Tidak ada data yang dapat ditampilkan.",
                info: "Menampilkan halaman _PAGE_ dari _PAGES_",
                infoEmpty: "Tidak ada data yang dapat ditampilkan",
                infoFiltered: " - difilter dari _MAX_ data",
                zeroRecords: "Tidak ada data yang dapat ditampilkan."
            },
            autoWidth: false
        });

    }

}).apply(this, [jQuery]);
