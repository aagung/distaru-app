SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `acl_resources` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('module','controller','action','other') NOT NULL DEFAULT 'other',
  `parent` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `acl_resources` (`id`, `name`, `type`, `parent`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'welcome', 'module', NULL, '2012-11-12 12:07:26', NULL, NULL, NULL),
(2, 'auth', 'module', NULL, '2012-11-12 04:00:23', NULL, NULL, NULL),
(3, 'auth/login', 'controller', 2, '2012-11-12 12:43:42', NULL, '2012-11-12 12:44:06', NULL),
(4, 'auth/logout', 'controller', 2, '2012-11-12 12:43:56', NULL, NULL, NULL),
(5, 'auth/user', 'controller', 2, '2012-11-12 04:07:59', NULL, '2012-11-12 08:29:29', NULL),
(6, 'acl', 'module', NULL, '2012-02-02 13:47:43', NULL, NULL, NULL),
(7, 'acl/resource', 'controller', 6, '2012-02-02 13:47:57', NULL, NULL, NULL),
(8, 'acl/resource/index', 'action', 7, '2012-02-02 13:48:21', NULL, NULL, NULL),
(9, 'acl/resource/add', 'action', 7, '2012-02-02 13:48:35', NULL, '2012-10-16 17:26:12', NULL),
(10, 'acl/resource/edit', 'action', 7, '2012-02-02 13:48:50', NULL, '2012-07-09 18:44:38', NULL),
(11, 'acl/resource/delete', 'action', 7, '2012-02-02 13:49:06', NULL, NULL, NULL),
(12, 'acl/role', 'controller', 6, '2012-07-12 17:54:16', NULL, NULL, NULL),
(13, 'acl/role/index', 'action', 12, '2012-07-12 17:55:29', NULL, NULL, NULL),
(14, 'acl/role/add', 'action', 12, '2012-07-12 17:56:00', NULL, NULL, NULL),
(15, 'acl/role/edit', 'action', 12, '2012-07-12 17:56:19', NULL, NULL, NULL),
(16, 'acl/role/delete', 'action', 12, '2012-07-12 17:56:55', NULL, NULL, NULL),
(17, 'acl/rule', 'controller', 6, '2012-07-12 17:53:04', NULL, NULL, NULL),
(18, 'acl/rule/edit', 'action', 17, '2012-07-12 17:53:25', NULL, NULL, NULL),
(19, 'utils', 'module', NULL, NULL, NULL, NULL, NULL),
(20, 'dashboard', 'module', NULL, NULL, NULL, NULL, NULL),
(21, 'api', 'module', NULL, NULL, NULL, NULL, NULL),
(22, 'samples', 'module', NULL, NULL, NULL, NULL, NULL),
(23, 'home', 'controller', NULL, NULL, NULL, NULL, NULL),
(24, 'mcs', 'module', NULL, '2016-08-05 22:14:14', 1704, NULL, NULL),
(25, 'mcs/data_peserta', 'controller', 24, NULL, NULL, NULL, NULL),
(26, 'mcs/verifikasi_medis', 'controller', 24, NULL, NULL, NULL, NULL),
(27, 'mcs/verifikasi_keuangan', 'controller', 24, NULL, NULL, NULL, NULL),
(28, 'mcs/reimbursement', 'controller', 24, NULL, NULL, NULL, NULL),
(29, 'mcs/his_pembayaran_klaim', 'controller', 24, NULL, NULL, NULL, NULL),
(30, 'mcs/bank', 'controller', 24, NULL, NULL, NULL, NULL),
(31, 'mcs/perusahaan', 'controller', 24, NULL, NULL, NULL, NULL),
(32, 'mcs/kategori_kartu', 'controller', 24, NULL, NULL, NULL, NULL),
(33, 'mcs/obat_alkes', 'controller', 24, NULL, NULL, NULL, NULL),
(34, 'mcs/layanan', 'controller', 24, NULL, NULL, NULL, NULL),
(35, 'mcs/satuan_kerja', 'controller', 24, NULL, NULL, NULL, NULL),
(36, 'mcs/instansi_kesehatan', 'controller', 24, NULL, NULL, NULL, NULL),
(37, 'mcs/poliklinik', 'controller', 24, NULL, NULL, NULL, NULL),
(38, 'mcs/dokter', 'controller', 24, NULL, NULL, NULL, NULL),
(39, 'mcs/pendaftaran_mcs', 'controller', 24, NULL, NULL, NULL, NULL),
(40, 'mcs/penagihan', 'controller', 24, NULL, NULL, NULL, NULL),
(41, 'igd', 'module', NULL, NULL, NULL, NULL, NULL),
(42, 'rawat_inap', 'module', NULL, NULL, NULL, NULL, NULL),
(43, 'ods_odc', 'module', NULL, NULL, NULL, NULL, NULL),
(44, 'farmasi', 'module', NULL, NULL, NULL, NULL, NULL),
(45, 'kasir', 'module', NULL, NULL, NULL, NULL, NULL),
(46, 'master', 'module', NULL, NULL, NULL, NULL, NULL),
(47, 'eis', 'module', NULL, '2017-05-11 06:24:41', 1704, NULL, NULL);

CREATE TABLE `acl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=26 DEFAULT CHARSET=utf8;

INSERT INTO `acl_roles` (`id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Administrator', '2011-12-27 12:00:00', NULL, NULL, NULL),
(2, 'Guest', '2011-12-27 12:00:00', NULL, NULL, NULL),
(3, 'Staf', '2012-11-12 04:30:02', 2012, NULL, NULL),
(4, 'Manager', '2012-11-12 04:30:24', NULL, NULL, NULL);

CREATE TABLE `acl_role_parents` (
  `role_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `acl_role_parents` (`role_id`, `parent`, `order`) VALUES
(3, 2, 0),
(4, 3, 0);

CREATE TABLE `acl_rules` (
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `access` enum('allow','deny') NOT NULL DEFAULT 'deny',
  `priviledge` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `acl_rules` (`role_id`, `resource_id`, `access`, `priviledge`) VALUES
(2, 1, 'allow', NULL),
(2, 3, 'allow', NULL),
(2, 4, 'allow', NULL),
(4, 2, 'allow', NULL),
(4, 5, 'allow', NULL);

CREATE TABLE `auth_autologin` (
  `user` int(11) NOT NULL,
  `series` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `auth_users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `registered` datetime NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `auth_users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `lang`, `registered`, `role_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1002, 'Diane', 'Murphy', 'dmurphy', 'dmurphy@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1056, 'Mary', 'Patterson', 'mpatterso', 'mpatterso@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1076, 'Jeff', 'Firrelli', 'jeff.firrelli', 'jeff.firrelli@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1088, 'William', 'Patterson', 'wpatterson', 'wpatterson@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1102, 'Gerard', 'Bondur', 'gbondur', 'gbondur@classicmodelcars.com', '$2a$08$/9GPAwtVkFug2y5yBIhmPOZWSev.Myt.ruNENXo9DT4VrqTwNBE2K', 'en', '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1143, 'Anthony', 'Bow', 'abow', 'abow@classicmodelcars.com', '$2a$08$w6grERmP9T3r7FOBAuxLjO0l9H05ZgFTgGUY26hA89/g/Wq.QLqye', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1165, 'Leslie', 'Jennings', 'ljennings', 'ljennings@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1166, 'Leslie', 'Thompson', 'lthompson', 'lthompson@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1216, 'Steve', 'Patterson', 'spatterson', 'spatterson@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1337, 'Loui', 'Bondur', 'lbondur', 'lbondur@classicmodelcars.com', '$2a$08$tGx5NElKJIm2hkX3OwRYSOp/VZ/r.oaB2YHdK.HBCDM921rfUVAta', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1370, 'Gerard', 'Hernandez', 'ghernande', 'ghernande@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1401, 'Pamela', 'Castillo', 'pcastillo', 'pcastillo@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1501, 'Larry', 'Bott', 'lbott', 'lbott@classicmodelcars.com', '$2a$08$Njus3nhJ9bX5YYGra6xRu.ldrTylOMebKHXW/Wfl0o2wMvtppY476', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1504, 'Barry', 'Jones', 'bjones', 'bjones@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1611, 'Andy', 'Fixter', 'afixter', 'afixter@classicmodelcars.com', '', 'en', '2012-03-01 05:54:30', 3, NULL, NULL, '2016-08-05 22:13:45', 1704),
(1612, 'Peter', 'Marsh', 'pmarsh', 'pmarsh@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1619, 'Tom', 'King', 'tking', 'tking@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1621, 'Mami', 'Nishi', 'mnishi', 'mnishi@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1625, 'Yoshimi', 'Kato', 'ykato', 'ykato@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1702, 'Martin', 'Gerard', 'mgerard', 'mgerard@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL, NULL, NULL, NULL),
(1703, 'Ardi', 'Soebrata', 'ardissoebrata', 'ardissoebrata@gmail.com', '$2a$08$KZRME/RCMM.ikhJvS9IQtOD/qQcM/922akreUjQ7fgL6BanTAwsIm', 'en', '2012-03-09 12:57:48', 4, NULL, NULL, NULL, NULL),
(1704, 'Administrator', 'Tea', 'admin', 'admin@example.com', '$2a$08$dxSn4NG3GUxu3XGLr4niIuemUHBohdWdBobNsRi6WpBE.h8zHNmXO', 'id', '2012-03-15 19:23:59', 1, NULL, NULL, NULL, NULL),
(1706, 'Test', 'TestLast', 'test', 'test@test.com', 'test', 'en', '2012-11-09 10:58:34', 2, NULL, NULL, NULL, NULL);

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `eis_rj_report01` (
  `id` int(10) UNSIGNED NOT NULL,
  `tahun` int(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `unitusaha_id` int(11) DEFAULT NULL,
  `kelompok_pasien_id` int(11) DEFAULT NULL,
  `kelompok_pasien` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `eis_rj_report02` (
  `id` int(10) UNSIGNED NOT NULL,
  `tahun` int(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `unitusaha_id` int(11) DEFAULT NULL,
  `layanan_id` int(11) DEFAULT NULL,
  `layanan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `eis_rj_report03` (
  `id` int(10) UNSIGNED NOT NULL,
  `tahun` int(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `unitusaha_id` int(11) DEFAULT NULL,
  `kecamatan_id` int(11) DEFAULT NULL,
  `wilayah` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `eis_rj_report04` (
  `id` int(10) UNSIGNED NOT NULL,
  `tahun` int(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `unitusaha_id` int(11) DEFAULT NULL,
  `icd_id` int(11) DEFAULT NULL,
  `diagnosa` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `m_unitusaha` (
  `id` int(11) NOT NULL,
  `uid` varchar(45) DEFAULT '',
  `kode` varchar(100) DEFAULT '',
  `nama` varchar(100) DEFAULT '',
  `kota` varchar(100) DEFAULT '',
  `lokasi` varchar(100) DEFAULT NULL,
  `negara` varchar(30) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `provinsi` varchar(30) DEFAULT NULL,
  `kode_pos` varchar(10) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `database` varchar(255) DEFAULT NULL,
  `last_used` datetime DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `created_at` datetime DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `m_unitusaha` (`id`, `uid`, `kode`, `nama`, `kota`, `lokasi`, `negara`, `phone`, `fax`, `email`, `alamat`, `provinsi`, `kode_pos`, `hostname`, `username`, `password`, `database`, `last_used`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '319a1800-57af-11e6-bd77-206a8a416890', '01', 'PT RSBT', 'Pangkal Pinang', 'Jl. Sudirman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'localhost', 'root', 'def502001b781b2d98f1570b52a0a0f9332645e8bb26796e59d1c09dcd9e66cec2a12d896f6a0482464f350ca08ffe5618350829d9565a79067d2b78e435dfe90ecf0f3aa9757d3ff49de0223ae08ff302b663ef9264cdd5fc564ecb50', 'imedis', '2017-09-15 04:00:08', 1, '2016-08-01 13:13:13', 0, '2017-09-15 04:00:08', NULL),
(2, '78cd79cb-57af-11e6-bd77-206a8a416890', '02', 'Rumah Sakit Bakti Timah Pangkalpinang (RSBT)', 'Pangkal Pinang', 'Jl. Bukit Baru No.1, Taman Bunga, Gerunggang, Kota Pangkal Pinang, Kepulauan Bangka Belitung 33684', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ptrsbt.com', 'root', 'def50200b30469efdfd5daff74937abee4148590a3d16b2196a2601c3f003f4b2f688486b409856acdfd4af8cb8a47e170a65cead5a7514383a8ac4224082468fcd14a34e78431aab70a093ba014092c5d5f56e2f4b453a4a918d5ce', 'imedis_pangkalpinang', '2017-09-15 04:00:11', 1, '2016-08-01 13:13:13', 0, '2017-09-15 04:00:11', NULL),
(3, 'e588a9ea-8ea1-11e6-b314-047d7bda3be5', '03', 'Rumah Sakit Tanjung Balai Karimun (RSTBK)', 'Karimun', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 'f576da2c-8ea1-11e6-b314-047d7bda3be5', '04', 'Rumah Sakit Medika Stannia (RSMS)', 'Sungai Liat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(5, 'fe5446e4-8ea1-11e6-b314-047d7bda3be5', '05', 'Rumah Sakit Bakti Timah Mentok', 'Mentok', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(6, '05171ed2-8ea2-11e6-b314-047d7bda3be5', '06', 'Klinik  Belinyu', 'Belinyu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(7, '1345593c-8ea2-11e6-b314-047d7bda3be5', '07', 'Klinik Parittiga-Jebus', 'Parit Tiga', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(8, '19a5ec6a-8ea2-11e6-b314-047d7bda3be5', '08', 'Klinik Toboali', 'Toboali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(9, '2120e8b5-8ea2-11e6-b314-047d7bda3be5', '09', 'Klinik Tanjung Pandan', 'Tanjung Pandan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);


ALTER TABLE `acl_resources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `parent` (`parent`);

ALTER TABLE `acl_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `acl_role_parents`
  ADD PRIMARY KEY (`role_id`,`parent`),
  ADD KEY `parent` (`parent`);

ALTER TABLE `acl_rules`
  ADD PRIMARY KEY (`role_id`,`resource_id`),
  ADD KEY `resource_id` (`resource_id`);

ALTER TABLE `auth_autologin`
  ADD PRIMARY KEY (`user`,`series`);

ALTER TABLE `auth_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`);

ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

ALTER TABLE `eis_rj_report01`
  ADD PRIMARY KEY (`id`),
  ADD KEY `periode` (`tahun`,`bulan`),
  ADD KEY `eis_rj_report01_unitusaha_id` (`unitusaha_id`);

ALTER TABLE `eis_rj_report02`
  ADD PRIMARY KEY (`id`),
  ADD KEY `periode` (`tahun`,`bulan`),
  ADD KEY `eis_rj_report01_unitusaha_id` (`unitusaha_id`);

ALTER TABLE `eis_rj_report03`
  ADD PRIMARY KEY (`id`),
  ADD KEY `periode` (`tahun`,`bulan`),
  ADD KEY `eis_rj_report03_unitusaha_id` (`unitusaha_id`);

ALTER TABLE `eis_rj_report04`
  ADD PRIMARY KEY (`id`),
  ADD KEY `periode` (`tahun`,`bulan`),
  ADD KEY `eis_rj_report04_unitusaha_id` (`unitusaha_id`);

ALTER TABLE `m_unitusaha`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `acl_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
ALTER TABLE `acl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
ALTER TABLE `auth_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1707;
ALTER TABLE `eis_rj_report01`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `eis_rj_report02`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `eis_rj_report03`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `eis_rj_report04`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `m_unitusaha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

ALTER TABLE `acl_role_parents`
  ADD CONSTRAINT `acl_role_parents_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_role_parents_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `acl_rules`
  ADD CONSTRAINT `acl_rules_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_rules_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `acl_resources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `auth_users`
  ADD CONSTRAINT `auth_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
