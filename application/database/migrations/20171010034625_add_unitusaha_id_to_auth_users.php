<?php

class Migration_add_unitusaha_id_to_auth_users extends CI_Migration
{
    public $fields = array(
        'unitusaha_id' => array(
            'type' => 'int',
            'default' => null,
            'after' => 'role_id'
        )
    );

    public function up()
    {
        $this->dbforge->add_column('auth_users', $this->fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('auth_users', $this->$fields);
    }
}