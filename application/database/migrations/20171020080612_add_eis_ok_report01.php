<?php

class Migration_add_eis_ok_report01 extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'tahun' => array(
                    'type' => 'int',
                    'default' => null,
                ),
                'bulan' => array(
                    'type' => 'int',
                    'default' => null,
                ),
                'unitusaha_id' => array(
                    'type' => 'int',
                    'default' => null,
                ),
                'total' => array(
                    'type' => 'int',
                    'default' => 0,
                ),
                'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            )
        );
        
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key(array('tahun', 'bulan'));
        $this->dbforge->add_key('unitusaha_id');
        $this->dbforge->create_table('eis_ok_report01');
    }
    
    public function down()
    {
        $this->dbforge->drop_table('eis_ok_report01');
    }
}