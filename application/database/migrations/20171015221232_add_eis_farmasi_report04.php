<?php

class Migration_add_eis_farmasi_report04 extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'int',
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'tahun' => array(
                    'type' => 'int',
                    'default' => null,
                ),
                'bulan' => array(
                    'type' => 'int',
                    'default' => null,
                ),
                'unitusaha_id' => array(
                    'type' => 'int',
                    'default' => null,
                ),
                'total_obat' => array(
                    'type' => 'int',
                    'default' => 0,
                ),
                'total_alkes' => array(
                    'type' => 'int',
                    'default' => 0,
                ),
                'updated_at DATETIME DEFAULT CURRENT_TIMESTAMP',
            )
        );
        
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key(array('tahun', 'bulan'));
        $this->dbforge->add_key('unitusaha_id');
        $this->dbforge->create_table('eis_farmasi_report04');
    }
    
    public function down()
    {
        $this->dbforge->drop_table('eis_farmasi_report04');
    }
}