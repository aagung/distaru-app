<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $template['metas']; ?>
        
        <link rel="icon" href="<?php echo assets_url('img/favlogo.png') ?>" type="image/png">
        <title><?php echo $template['title']; ?></title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo css_url('icons/icomoon/styles.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('bootstrap.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('core.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('components.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('colors.css') ?>" rel="stylesheet">
        <link href="<?php echo css_url('vmt.custom.css') ?>" rel="stylesheet">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/pace.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo js_url('core/app.min.js') ?>"></script>
        <!-- /core JS files -->

        <script type="text/javascript" src="<?php echo js_url('core/app.js') ?>"></script>
        <!-- /theme JS files -->

    </head>
	<style type="text/css">
		body {
			background-image: url("<?php echo base_url('assets/img/background.png'); ?>");
  			background-color: #b2e2f4;
  			background-attachment: fixed;
			background-repeat: no-repeat;
			background-position: center bottom;
    		background-size: 100% auto;
    		color: #DDDDDD;
		}
		.content-wrapper {
			vertical-align: middle !important;
		}
		#logo {
			font-family: 'Roboto', serif;
        	font-size: 48px;
		}
	</style>
    <body class="login-container">
		<!-- /main navbar -->
		
		<!-- Page container -->
		<div class="page-container">

			<!-- Page content -->
			<div class="page-content">

				<!-- Main content -->
				<div class="content-wrapper">

					<!-- Content area -->
					<div class="content">

						<?php echo $template['content']; ?>

						<!-- Footer -->
						<div class="footer text-center">
							&copy; <?php echo $this->config->item('footer_label'); ?>
						</div>
						<!-- /footer -->

					</div>
					<!-- /content area -->

				</div>
				<!-- /main content -->

			</div>
			<!-- /page content -->

		</div>
		<!-- /page container -->

    </body>
</html>
