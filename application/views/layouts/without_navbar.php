<?php
$name = '';
$role = '';
if ($this->auth->loggedin()) {
    $name = $auth_user['name'];
    $role = $auth_user['role_name'];
    if (strlen(trim($name)) == 0) {
        $name = $auth_user['username'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $template['metas']; ?>

        <title><?php echo $template['title']; ?></title>

        <link rel="icon" href="<?php echo assets_url('img/favlogo.png'); ?>" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo assets_url('show_app/css/normalize.css'); ?>" type="text/css"><!-- CSS reset -->
        <link rel="stylesheet" href="<?php echo assets_url('show_app/css/material-font.min.css'); ?>" type="text/css"><!-- Font Icon -->
        <link rel="stylesheet" href="<?php echo assets_url('show_app/css/animate.min.css'); ?>" type="text/css"><!-- Animate -->
        <link rel="stylesheet" href="<?php echo assets_url('show_app/css/main.css'); ?>" type="text/css"><!-- Main -->
        <!-- Resource style -->

        <script src="<?php echo assets_url('show_app/bundles/libscripts.bundle.js'); ?>"></script> 
        <script src="<?php echo assets_url('show_app/bundles/vendorscripts.bundle.js'); ?>"></script> 
        <script src="<?php echo assets_url('show_app/js/main.js'); ?>"></script> <!-- Main Script -->
    </head>
    <body>
        <style type="text/css">
            .float {
                position:fixed;
                width:60px;
                height:40px;
                bottom:40px;
                right:50%;
                color:#FFF;
                border-radius:50px;
                text-align:center;
                box-shadow: 2px 2px 3px #999;
            }
        </style>
        <div id="main-nav" style="padding: 0;">
            <div id="slide_out_menu"> 
                <a href="javascript:void(0);" class="menu-close"><i class="zmdi zmdi-close"></i></a>
                <div class="logo"><a href="javascript:void(0);"><img src="<?php echo assets_url('img/logo-header.png'); ?>" width="300" height="50"></a></div>
                <ul>
                    <?php foreach ($list_app as $row): ?>
                    <li>
                        <a href="<?php echo site_url('app/app/show/'.$row->uid); ?>">
                         <?php echo ucwords($row->name); ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                    <li><a href="<?php echo $back_button; ?>" class="btn btn-info btn-round">Kembali</a></li>
                </ul>
                <div class="slide_out_menu_footer">
                    <!-- <div class="more-info">
                        <p>Powered by <a href="#">VMT Software</a></p>
                    </div>
                    <ul class="socials">
                        <li><a href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a></li>
                        <li><a href="javascript:void(0);"><i class="zmdi zmdi-facebook"></i></a></li>
                        <li><a href="javascript:void(0);"><i class="zmdi zmdi-google-plus"></i></a></li>
                        <li><a href="javascript:void(0);"><i class="zmdi zmdi-tumblr"></i></a></li>
                        <li><a href="javascript:void(0);"><i class="zmdi zmdi-pinterest"></i></a></li>
                        <li><a href="javascript:void(0);"><i class="zmdi zmdi-linkedin"></i></a></li>
                    </ul> -->
                </div>
            </div>
            <button id="navigation" class="btn btn-info btn-round float">
                <span class="text-warning"><i class="zmdi zmdi-apps"></i></span>
            </button>
        </div>
        <div class="content">   
            <?php echo $template['content']; ?>
        </div>      
        <?php
        if (isset($template['script']) && $template['script'] != '')
            $this->load->view($template['script']);
        ?>
    </body>
</html>
