<?php
$name = '';
$role = '';
if ($this->auth->loggedin()) {
    $name = $auth_user['name'];
    $role = $auth_user['role_name'];
    if (strlen(trim($name)) == 0) {
        $name = $auth_user['username'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $template['metas']; ?>

        <title><?php echo $template['title']; ?></title>

        <link rel="icon" href="<?php echo assets_url('img/favlogo.png') ?>" type="image/png">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->
        <?php echo $template['css']; ?>
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/pace.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/jquery.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('core/libraries/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/loaders/blockui.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('plugins/forms/selects/select2.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/uniform.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switchery.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo js_url('plugins/forms/styling/switch.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo bower_url('moment/min/moment-with-locales.min.js') ?>"></script>
		<script type="text/javascript" src="<?php echo bower_url('numeraljs/min/numeral.min.js') ?>"></script>
		<script>
			base_url = '<?php echo site_url() ?>';
			assets_url = '<?php echo assets_url() ?>';
			moment.locale('id');
		</script>
        <!-- /core JS files -->

        <!-- /theme JS files -->
        <?php echo $template['js_header']; ?>
        <script type="text/javascript" src="<?php echo js_url('core/app.js') ?>"></script>
        <script type="text/javascript" src="<?php echo js_url('app.js') ?>"></script>
    </head>
    <style type="text/css">
    	#logo {
			font-family: 'Roboto', serif;
        	font-size: 35px;
        	margin: 0;
        	position: absolute;
        	top: -2px;
		}
    </style>
    <body class="navbar-top has-detached-right" data-spy="scroll" data-target=".sidebar-detached" data-offset="100">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo site_url() ?>">
                	<h3 id="logo"><img src="<?php echo assets_url('img/logo-header.png'); ?>" width="180" height="40"></h3>
                </a>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">


                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo image_url('placeholder.jpg') ?>" alt="">
                            <span><?php echo $name ?></span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?php echo site_url('auth/user/profile?redirect='.  urlencode(current_url())); ?>"><i class="icon-user-plus"></i> My profile</a></li>
                            <li><a href="<?php echo site_url('auth/logout') ?>"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main sidebar-fixed">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="<?php echo image_url('placeholder.jpg') ?>" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold"><?php echo $name ?></span>
                                        <div class="text-size-mini text-muted">
                                             <i class="icon-vcard text-size-small"></i> &nbsp;<?php echo $role ?>
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="#"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">
                                    <?php
									function set_active($menus, $curr_uri, $acl)
									{
										foreach($menus as $index => $menu) {
											$is_active = false;
											$is_allowed = false;
											$has_children = isset($menu['children']) and is_array($menu['children']);
											if ($has_children) {
												$menus[$index]['children'] = set_active($menus[$index]['children'], $curr_uri, $acl);
												foreach($menus[$index]['children'] as $menu_item) {
													if ($menu_item['is_active']) {
														$is_active = $is_active || true;
													}
													if ($menu_item['is_allowed']) {
														$is_allowed = $is_allowed || true;
													}
												}
											} else {
												$is_active = strpos($curr_uri, $menu['uri']) === 0;
												$is_allowed = !isset($menu['uri']) || $acl->is_allowed($menu['uri']);
											}
											$menus[$index]['is_active'] = $is_active;
											$menus[$index]['is_allowed'] = $is_allowed;
										}
										return $menus;
									}
									
									function display_menu_item($menu, $curr_uri)
									{
										if (empty($curr_uri)) $curr_uri = 'home';
										$is_active = (isset($menu['is_active']) && $menu['is_active']);
										$is_allowed = (isset($menu['is_allowed']) && $menu['is_allowed']);
										$has_children = isset($menu['children']) and is_array($menu['children']);
										if ($is_allowed) {
											echo '<li' . ($is_active ? ' class="active"' : '') . '>';
											echo '<a href="' . ((isset($menu['uri']) and !empty($menu['uri'])) ? site_url($menu['uri']) : '#') . '"' . ($has_children ? ' class="has-ul"' : '') . '>';
											if (isset($menu['icon']) && !empty($menu['icon']))
												echo '<i class="' . $menu['icon'] . '"></i>';
											echo '<span>' . $menu['title'] . '</span>';
											echo '</a>';
											if ($has_children) {
												echo '<ul' . ($is_active ? '' : ' class="hidden-ul"') . '>';
												foreach($menu['children'] as $menu_item) {
													display_menu_item($menu_item, $curr_uri);
												}
												echo '</ul>';
											}
										}
									}
									
									$curr_uri = $this->uri->uri_string();
									if (empty($curr_uri)) $curr_uri = 'home';
                                    $this->load->config('navigation');
                                    $navigation = set_active($this->config->item('navigation'), $curr_uri, $this->acl);
									foreach($navigation as $menu) {
										display_menu_item($menu, $curr_uri);
									}
									?>
                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

					<!-- Page header -->
					<div class="page-header page-header-default">
						<div class="page-header-content">
							<div class="page-title">
								<h4><?php echo $page_title ?></h4>
							</div>
							
							<div class="heading-elements">
							<?php if (isset($page_icons) && !empty($page_icons)): ?>
								<div class="heading-btn-group">
									<?php echo $page_icons ?>
								</div>
							<?php endif; ?>
							<?php if (isset($charts) && !empty($charts)): ?>
								<form class="heading-form" action="#">
									<div class="form-group">
										<select id="chart-menu" name="chart-menu" class="select2">
											<?php foreach($charts as $index => $chart): ?>
											<option value="<?php echo '#panel-chart-' . sprintf('%02d', $index) ?>"><?php echo $chart['title'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</form>
							<?php endif; ?>
							</div>
							<a class="heading-elements-toggle"><i class="icon-more"></i></a>
						</div>
					</div>
					<!-- /page header -->

                    <!-- Content area -->
                    <div id="vmt_content" class="content" data-spy="scroll" data-target="#chart-menu">
						
                        <?php echo $template['content']; ?>
					
						<div class="footer text-muted">
							&copy; <?php echo $this->config->item('footer_label'); ?>
						</div>
                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
        <?php echo $template['js_footer']; ?>
		
        <?php
        if (isset($template['script']) && $template['script'] != '')
            $this->load->view($template['script']);
        ?>
		
		<script>
			$(window).load(function() {
				
				$('select.select2').select2({
					minimumResultsForSearch: 10,
					width: '100%'
				});

				// External table additions
				// ------------------------------

				// Enable Select2 select for the length option
				$('.dataTables_length select').select2({
					minimumResultsForSearch: 10,
					width: 'auto'
				});

				// Primary
				$(".control-primary").uniform({
					radioClass: 'choice',
					wrapperClass: 'border-primary-600 text-primary-800'
				});

				// Danger
				$(".control-danger").uniform({
					radioClass: 'choice',
					wrapperClass: 'border-danger-600 text-danger-800'
				});

				// Success
				$(".control-success").uniform({
					radioClass: 'choice',
					wrapperClass: 'border-success-600 text-success-800'
				});

				// Warning
				$(".control-warning").uniform({
					radioClass: 'choice',
					wrapperClass: 'border-warning-600 text-warning-800'
				});

				// Info
				$(".control-info").uniform({
					radioClass: 'choice',
					wrapperClass: 'border-info-600 text-info-800'
				});
			});
		</script>
    </body>
</html>
