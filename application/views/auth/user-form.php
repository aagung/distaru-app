<?php echo messages(); ?>
<div class="row">
	<div class="col-md-12">
		<form id="user-form" class="form-horizontal normalcase" method="post">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo lang('account'); ?></h3>
				</div>
				<div class="panel-body">
					<?php echo $form->fields(); ?>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn pull-right">
							<button type="submit" name="save-button" value="Simpan" id="save-button" class="btn-success btn-labeled btn">
								<b><i class="icon-floppy-disk"></i></b>
								Simpan
							</button>
							<input type="submit" name="cancel-button" value="Batal" id="cancel-button" class="btn btn-default">
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	$(window).load(function() {
		$('#top-save-btn').on('click', function(e) {
			e.preventDefault();
			$('#save-button').click();
		});
	});
</script>