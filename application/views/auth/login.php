<form role="form" method="post">
	<div class="panel panel-body login-form normalcase">
		<div class="text-center">
			<h3 id="logo"><img src="<?php echo assets_url('img/logo.png'); ?>" width="250" height="50"></h3>
			<h5 class="content-group">Verifikasi Keamanan<small class="display-block">Silahkan login terlebih dahulu</small></h5>
		</div>
		
		<?php echo messages(); ?>

		<div class="form-group has-feedback has-feedback-left">
			<input type="text" class="form-control" placeholder="<?php echo lang('id_card'); ?>" name="username" autofocus value="<?php echo (isset($id_card)) ? $id_card : ''; ?>">
			<div class="form-control-feedback">
				<i class="icon-user text-muted"></i>
			</div>
		</div>
		
		<div class="form-group has-feedback has-feedback-left">
			<input type="password" class="form-control" placeholder="<?php echo lang('password'); ?>" name="password"  value="" />
			<div class="form-control-feedback">
				<i class="icon-lock2 text-muted"></i>
			</div>
		</div>

		<div class="form-group">
			<button  name="login-button" type="submit" class="btn bg-primary-800 btn-block"><i class="icon-key position-left"></i><?php echo lang('login'); ?> </button>
		</div>

	</div>
</form>
