<?php echo messages(); ?>
<div class="panel panel-flat">
	<div class="panel-body">
		<table id="dataTable_user" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th><?php echo lang('first_name'); ?></th>
					<th><?php echo lang('last_name'); ?></th>
					<th><?php echo lang('username'); ?></th>
					<th><?php echo lang('email'); ?></th>
					<th>Role</th>
					<th class="text-right"><?php echo lang('registered'); ?></th>
					<th style="width: 15px;"></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<?php $this->load->view('delete-modal'); ?>

<script>
var the_table;

$(window).load(function() {
	the_table = $("#dataTable_user").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?php echo site_url('api/user/index'); ?>",
                "type": "POST"
            },
			"columns": [
					{ "data": "first_name" },
					{ "data": "last_name" },
					{ 
						"data": "username",
						"render": function(data, type, row, meta) {
							return '<a href="<?php echo site_url('auth/user/edit/') ?>/' + row.id + '">' + data + '</a>';
						}
					},
					{ "data": "email" },
					{ "data": "role" },
					{ 
						"data": "registered",
						"render": function(data, type, row, meta) {
							return moment(data).isValid() ? moment(data).format('DD-MM-YYYY') : data;
						},
						"className": "text-right"
					},
					{ 
						"data": "action",
						"orderable": false,
						"render": function(data, type, row, meta) {
							return '<a href="<?php echo site_url('auth/user/delete/') ?>/' + row.id + '" title="<?php echo lang('delete') ?>" data-button="delete"><i class="fa fa-trash-o"></i></a>';
						}
					}
			]
        });
});
</script>