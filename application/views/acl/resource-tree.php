
<?php if (isset($form)) $resource = $form->get_default(); ?>
<?php echo messages(); ?>
<div class="row">
	<div class="col-md-5">
		<section class="panel panel-default">
			<div class="panel-body">
				<?php
				function display_tree($tree, $acl, $curr_id = 0)
				{
					foreach($tree as $node) {
						if ($curr_id == $node['id'])
							echo '<li class="active"><strong>';
						else
							echo '<li>';
						$class = 'fa ';
						switch($node['type']) {
							case 'module':
								$class .= 'fa-folder-open-o ';
								break;
							case 'controller':
								$class .= 'fa-file-text-o';
								break;
							case 'action':
								$class .= 'fa-gear';
								break;
							default:
								$class .= 'fa-question';
								break;
						}
						echo '<i class="fa-li ' . $class . '"></i>';
						if($acl->is_allowed('acl/resource/edit')){
							echo '<a href="' . site_url('acl/resource/edit/' . $node['id']) . '">';
							echo $node['name'];
							echo '</a>';
						} else {
							echo $node['name'];
						}
						if (isset($node['children'])) {
							echo '<ul class="fa-ul">';
							display_tree($node['children'], $acl, $curr_id);
							echo '</ul>';
						}
						if ($curr_id == $node['id'])
							echo '</strong>';
						echo '</li>';
					}
				}
				?>
				<ul class="fa-ul">
					<?php display_tree($resource_tree, $acl, (isset($resource->id) ? $resource->id : 0)); ?>
				</ul>
			</div>
		</section>
	</div>
	<?php if (isset($form)): ?>
	<div class="col-md-7">
		<?php echo form_open_multipart(uri_string(), array('class' => 'form-horizontal form-bordered normalcase', 'id' => 'resource-form', 'name' => 'resource-form')); ?>
			<section class="panel panel-white">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo lang('resource_page_name') ?></h3>
				</div>
				<div class="panel-body">
					<?php echo $form->fields(array('id', 'name', 'type')) ?>
					<div class="form-group">
						<?php echo form_label(lang('resource_parent'), 'parent', array('class' => 'col-lg-4 control-label')); ?>
						<div class="col-lg-8">
						<?php
						function generate_options($tree, $sep = '')
						{
							$result = array();
							foreach($tree as $node)
							{
								$result[$node['id']] = $sep . $node['type'] . '&nbsp;' . $node['name'];
								if (isset($node['children']))
									$result = $result + generate_options($node['children'], $sep . '&nbsp;&nbsp;&nbsp;&nbsp;');
							}
							return $result;
						}
						$parents = array(0 => '(' . lang('resource_parent_none') . ')') + generate_options($resource_tree);
						if (isset($resource->id) && isset($parents[$resource->id]))
							unset($parents[$resource->id]);

						echo form_dropdown('parent', 
							$parents, 
							set_value('parent', isset($resource->parent) ? $resource->parent : 0),
							'id="parent" class="form-control"'
						);
						?>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<div class="heading-btn">
							<?php 
							if($acl->is_allowed('acl/resource/edit')) {
								echo form_button(array(
									'type' => 'submit',
									'name' => 'save_task',
									'value' => 'save',
									'content' => '<b><i class="icon-floppy-disk"></i></b> ' . lang('save'),
									'class' => 'btn btn-success btn-labeled'
								));
							}
							?>
							<a href="<?php echo site_url('acl/resource'); ?>" class="btn btn-default">
								<?php echo lang('back') ?>
							</a>
						</div>
						<div class="heading-btn pull-right">
							<?php
							if (isset($resource->id) && $acl->is_allowed('acl/resource/delete')) {
								$delete_url = site_url('acl/resource/delete/' . $resource->id);
								echo form_confirmwindow('delete-confirm', '<b><i class="fa fa-trash-o"></i></b> ' . lang('delete'), lang('delete'), lang('resource_delete_confirm'), $delete_url, 'btn btn-danger btn-labeled pull-right', 'btn btn-danger');
							}
							?>
						</div>
					</div>
				</div>
			</section>
		<?php echo form_close(); ?>
	</div>
	<?php endif; ?>
</div>

<script>
	$(document).ready(function() {
		$('#type').select2({
			minimumResultsForSearch: 20,
			escapeMarkup: function (markup) { return markup; },
			templateResult: function(row) {
				return render_type(row.text);
			},
			templateSelection: function(row) {
				return render_type(row.text);
			}
		});
		$('#parent').select2({
			minimumResultsForSearch: 20,
			escapeMarkup: function (markup) { return markup; },
			templateResult: function(row) {
				return render_parent(row.text);
			},
			templateSelection: function(row) {
				return render_parent(row.text);
			}
		});
	});
	
	function render_type(text) {
		switch (text) {
			case 'Module':
				return '<i class="fa fa-folder-open-o"></i>' + text;
				break;
			case 'Controller':
				return '<i class="fa fa-file-text-o"></i>' + text;
				break;
			case 'Action':
				return '<i class="fa fa-gear"></i>' + text;
				break;
			default:
				return '<i class="fa fa-question"></i>' + text;
				break;
		}
	}
	
	function render_parent(text) {
		return text.replace('module', '<i class="fa fa-folder-open-o"></i>')
				.replace('controller', '<i class="fa fa-file-text-o"></i>')
				.replace('action', '<i class="fa fa-gear"></i>')
				.replace('other', '<i class="fa fa-question"></i>');
	}
</script>