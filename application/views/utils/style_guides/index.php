<style>
	body {
		position: relative;
	}
	.content-group .page-header {
		position: static;
	}
	.heading-elements {
		right: 20px;
	}
</style>

<div class="panel panel-white">
	<div class="panel-heading">
		<h2 id="standar_warna_buttons" class="panel-title"><i class="icon-grid5 position-left"></i>Standar Warna & Buttons</h2>
	</div>
	<div class="panel-body">
		<h3 id="btn_primary" class="no-margin-top">btn-primary</h3>
		<p>Gunakan untuk aksi yang <strong>bukan</strong> bagian dari aksi utama atau alur utama kerja. Atau untuk aksi-aksi pilihan/opsional. Aksi ini tidak akan mengubah status data.</p>
		<p><button type="button" class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</button></p>
		<pre class="language-php content-group"><code>&lt;button type=&quot;button&quot; class=&quot;btn btn-primary btn-labeled&quot;&gt;
	&lt;b&gt;&lt;i class=&quot;icon-plus-circle2&quot;&gt;&lt;/i&gt;&lt;/b&gt;
	Tambah
&lt;/button&gt;</code></pre>
		<h3 id="btn_success">btn-success</h3>
		<p>Gunakan untuk aksi yang merupakan bagian dari <strong>aksi utama</strong> atau alur utama kerja. Aksi ini akan mengubah status data. Setelah User melakukan suatu pekerjaan, dia akan mencari tombol ini untuk melanjutkan ke langkah selanjutnya.</p>
		<p><button type="button" class="btn btn-success btn-labeled"><b><i class="icon-floppy-disk"></i></b>Simpan</button></p>
		<pre class="language-php content-group"><code>&lt;button type=&quot;button&quot; class=&quot;btn btn-success btn-labeled&quot;&gt;
	&lt;b&gt;&lt;i class=&quot;icon-floppy-disk&quot;&gt;&lt;/i&gt;&lt;/b&gt;
	Simpan
&lt;/button&gt;</code></pre>
		<h3 id="btn_danger">btn-danger</h3>
		<p>Gunakan untuk aksi yang bersifat <strong>destruktif</strong>/merusak. Aksi ini akan mengubah status data. Ketika User meng-klik tombol ini, popup konfirmasi akan ditampilkan untuk memastikan apakah User akan melanjutkan proses.</p>
		<p><button type="button" data-button="delete" class="btn btn-danger btn-labeled"><b><i class="icon-trash"></i></b>Hapus</button></p>
		<pre class="language-php content-group"><code>&lt;button type=&quot;button&quot; data-button=&quot;delete&quot; class=&quot;btn btn-danger btn-labeled&quot;&gt;&lt;b&gt;
	&lt;i class=&quot;icon-trash&quot;&gt;&lt;/i&gt;&lt;/b&gt;
	Hapus
&lt;/button&gt;</code></pre>
		<h3 id="btn_default">btn-default</h3>
		<p>Gunakan untuk aksi yang bersifat membatalkan aksi utama atau alur utama kerja. Aksi ini tidak akan mengubah status data.</p>
		<p><strong>Catatan:</strong> Gunakan "<strong>Kembali</strong>" untuk membatalkan aksi, jangan menggunakan kata "<strong>Batal</strong>". Hal ini untuk menghindari kerancuan dengan istilah "<strong>Batal</strong>" yang digunakan untuk status data yang di-batal-kan.</p>
		<p><button type="button" class="btn btn-default">Kembali</button></p>
		<pre class="language-php content-group"><code>&lt;button type=&quot;button&quot; class=&quot;btn btn-default&quot;&gt;&lt;b&gt;
	Kembali
&lt;/button&gt;</code></pre>
		<blockquote class="no-margin">
			<i class="icon-alert pull-left icon-2x mr-20"></i>
			Gunakan warna dengan bijak, jangan sampai membingungkan User karena penggunaan warna yang tidak sesuai. Silakan gunakan warna-warna lain jika dibutuhkan dan dapat membantu memperjelas maksud dari pemakaian aplikasi.
		</blockquote>
	</div>
</div>

<div class="panel panel-white">
	<div class="panel-heading">
		<h2 id="page_header" class="panel-title"><i class="icon-browser position-left"></i>Page Header</h2>
	</div>
	<div class="panel-body">
		<p>Page Header berisi 2 bagian, yaitu Bagian <strong>Title</strong> dan Bagian <strong>Action</strong>.</p>
		<h3 id="page_header_title">Page Header Title</h3>
		<p>Bagian <strong>Title</strong> berisi icon dan judul halaman.</p>
		<p>Icon yang digunakan harus sama dengan icon yang digunakan pada menu sidebar, atau gunakan icon menu parent jika menu yang digunakan tidak memiliki icon.</p>
		<p><strong>Contoh Header</strong></p>
		<div class="content-group" style="border-color: #ddd; border-style: solid; border-width: 1px 1px 0 1px;">
			<div class="page-header page-header-default border-grey">
				<div class="page-header-content">
					<div class="page-title">
						<h4><?php echo $page_title ?></h4>
					</div>
				</div>
			</div>
		</div>
		<p><strong>Title</strong> diset pada controller, semua view pada controller tersebut akan memiliki title yang sama.</p>
		<pre class="language-php content-group"><code>class Style_guides extends Admin_Controller
{
	protected $page_title = '&lt;i class="fa fa-wrench"&gt;&lt;/i&gt;Style Guides';
}</code></pre>
		<p>Atau jika ingin title yang berbeda untuk view tertentu, kirim variabel bernama <code>$page_title</code> ke view tersebut.</p>
		<pre class="language-php content-group"><code>function index()
{
	$this->template->build('utils/style_guides/index', array('page_title' => '&lt;i class="fa fa-wrench"&gt;&lt;/i&gt;Style Guides');
}</code></pre>

		<h3 id="page_header_action">Page Header Action</h3>
		<p>Bagian <strong>Action</strong> berisi button-button aksi utama untuk halaman tersebut. Untuk halaman DataTable biasanya bagian ini berisi tombol <strong>Tambah</strong> dan untuk halaman Form berisi tombol <strong>Simpan</strong> dan <strong>Kembali</strong>.</p>
		<p><strong>Contoh Header Action DataTable</strong></p>
		<div class="content-group" style="border-color: #ddd; border-style: solid; border-width: 1px 1px 0 1px;">
			<div class="page-header page-header-default border-grey">
				<div class="page-header-content">
					<div class="page-title">
						<h4>DataTable</h4>
					</div>
					<div class="heading-elements">
						<div class="heading-btn-group">
							<a class="btn btn-primary btn-labeled"><b><i class="icon-plus-circle2"></i></b>Tambah</a>
						</div>
					</div>
					<a class="heading-elements-toggle"><i class="icon-more"></i></a>
				</div>
			</div>
		</div>
		<p><strong>Action</strong> diset pada controller. Kirim variabel bernama <code>$page_icons</code> ke view controller tersebut.</p>
		<pre class="language-php content-group"><code>function index()
{
	$this->template->build('index', array('page_icons' => '&lt;a href=&quot;&quot; class=&quot;btn btn-primary btn-labeled&quot;&gt;&lt;b&gt;&lt;i class=&quot;icon-plus-circle2&quot;&gt;&lt;/i&gt;&lt;/b&gt;Tambah&lt;/a&gt;');
}</code></pre>
		<p><strong>Contoh Header Action Form</strong></p>
		<div class="content-group" style="border-color: #ddd; border-style: solid; border-width: 1px 1px 0 1px;">
			<div class="page-header page-header-default border-grey">
				<div class="page-header-content">
					<div class="page-title">
						<h4>Form</h4>
					</div>
					<div class="heading-elements">
						<div class="heading-btn-group">
							<a id="top-save-btn" href="" class="btn btn-success btn-labeled"><b><i class="icon-floppy-disk"></i></b>Simpan</a>
							<a id="top-cancel-btn" href="" class="btn btn-default">Batal</a>
						</div>
					</div>
					<a class="heading-elements-toggle"><i class="icon-more"></i></a>
				</div>
			</div>
		</div>
		<p>Untuk form agar tombol <strong>Action</strong> dapat melakukan submit form, buat script javascript yang akan meng-klik tombol submit pada form yang bersangkutan</p>
		<pre class="language-php content-group"><code>function edit()
{
	$this->template->build('form', array('page_icons' => '&lt;a id=&quot;top-save-btn&quot; href=&quot;&quot; class=&quot;btn btn-success btn-labeled&quot;&gt;&lt;b&gt;&lt;i class=&quot;icon-floppy-disk&quot;&gt;&lt;/i&gt;&lt;/b&gt;Simpan&lt;/a&gt;&lt;a id=&quot;top-cancel-btn&quot; href=&quot;&quot; class=&quot;btn btn-default&quot;&gt;Batal&lt;/a&gt;');
}</code></pre>
		<pre class="language-javascript content-group"><code>$(window).load(function() {
	$('#top-save-btn').on('click', function(e) {
		e.preventDefault();
		$('#save-button').click();
	});
});</code></pre>
	</div>
</div>

<div class="panel panel-white">
	<div class="panel-heading">
		<h2 id="datatable" class="panel-title"><i class="icon-table2 position-left"></i>DataTable</h2>
	</div>
	<div class="panel-body">
		<table id="dataTable_user" class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>Nama</th>
					<th>ID</th>
					<th class="text-right">Tanggal</th>
					<th class="text-right">Jumlah</th>
					<th class="text-right">Saldo</th>
					<th style="width: 15px;"></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<hr>
		<p>Pastikan link ke script <code>datatables.min.js</code> disertakan dalam controller.</p>
		<pre class="language-php content-group"><code>public function index() 
{
	$this-&gt;template
		-&gt;set_js(&#39;plugins/tables/datatables/datatables.min&#39;, FALSE)
		-&gt;build(&#39;index&#39;);
}</code></pre>
		<p>Gunakan tag table standar untuk menampilkan tabel dalam view:</p>
		<pre class="language-php content-group"><code>&lt;table id=&quot;dataTable_user&quot; class=&quot;table table-bordered table-striped table-hover&quot;&gt;
	&lt;thead&gt;
		&lt;tr&gt;
			&lt;th&gt;Nama&lt;/th&gt;
			&lt;th&gt;ID&lt;/th&gt;
			&lt;th class=&quot;text-right&quot;&gt;Tanggal&lt;/th&gt;
			&lt;th class=&quot;text-right&quot;&gt;Jumlah&lt;/th&gt;
			&lt;th class=&quot;text-right&quot;&gt;Saldo&lt;/th&gt;
			&lt;th style=&quot;width: 15px;&quot;&gt;&lt;/th&gt;
		&lt;/tr&gt;
	&lt;/thead&gt;
	&lt;tbody&gt;
	&lt;/tbody&gt;
&lt;/table&gt;

&lt;?php $this-&gt;load-&gt;view(&#39;delete-modal&#39;); ?&gt;

&lt;script&gt;
var the_table;

$(window).load(function() {
	the_table = $(&quot;#dataTable_user&quot;).DataTable({
			&quot;processing&quot;: true,
			&quot;serverSide&quot;: true,
			&quot;ajax&quot;: {
				&quot;url&quot;: &quot;&lt;?php echo site_url(&#39;datatable_ajax&#39;); ?&gt;&quot;,
				&quot;type&quot;: &quot;POST&quot;
			},
			&quot;columns&quot;: [
					{ 
						&quot;data&quot;: &quot;nama&quot;,
						&quot;render&quot;: function(data, type, row, meta) {
							return &#39;&lt;a href=&quot;#&quot;&gt;&#39; + data + &#39;&lt;/a&gt;&#39;;
						}
					},
					{ &quot;data&quot;: &quot;no_id&quot; },
					{ 
						&quot;data&quot;: &quot;tgl.date&quot;,
						&quot;render&quot;: function(data, type, row, meta) {
							return moment(data).isValid() ? moment(data).format(&#39;DD-MM-YYYY&#39;) : data;
						},
						&quot;className&quot;: &quot;text-right&quot;
					},
					{
						&quot;data&quot;: &quot;jml&quot;,
						&quot;render&quot;: function(data, type, row, meta) {
							return numeral(data).format(&#39;0,0&#39;);
						},
						&quot;className&quot;: &quot;text-right&quot;
					},
					{
						&quot;data&quot;: &quot;saldo&quot;,
						&quot;render&quot;: function(data, type, row, meta) {
							return numeral(data).format(&#39;$0,0.00&#39;);
						},
						&quot;className&quot;: &quot;text-right&quot;
					},
					{ 
						&quot;data&quot;: &quot;action&quot;,
						&quot;orderable&quot;: false,
						&quot;render&quot;: function(data, type, row, meta) {
							return &#39;&lt;a href=&quot;#&quot; title=&quot;&lt;?php echo lang(&#39;delete&#39;) ?&gt;&quot; data-button=&quot;delete&quot;&gt;&lt;i class=&quot;fa fa-trash-o&quot;&gt;&lt;/i&gt;&lt;/a&gt;&#39;;
						}
					}
			]
		});
});
&lt;/script&gt;
</code></pre>
		<blockquote class="no-margin">
			<i class="icon-alert pull-left icon-2x mr-20"></i>
			Jangan mem-format data tanggal dan angka di sisi server (PHP). Adalah tugas DataTable yang melakukan format data tanggal dan angka.
			Gunakan <a href="http://momentjs.com/">momentjs</a> untuk memformat tanggal dan <a href="http://numeraljs.com/">numeraljs</a> untuk memformat angka.
		</blockquote>
	</div>
</div>

<div class="panel panel-white">
	<div class="panel-heading">
		<h2 id="menu_icons" class="panel-title"><i class="icon-grid-alt position-left"></i>Menu Icons</h2>
	</div>
	<div class="panel-body">
		<p>Icon medis diambil dari <a href="https://github.com/samcome/webfont-medical-icons">https://github.com/samcome/webfont-medical-icons</a>. Karena nama class yang digunakan konflik dengan nama class iconmoon, class icon-icon ini telah diubah dengan awalan <code>icon-med</code>.</p>
		<?php
		$url_list = array(
			array(
				'uri' => 'pendaftaran',
				'title' => 'Pendaftaran',
				'icon' => 'icon-med-i-registration'
			),
			array(
				'uri' => 'rawat_jalan',
				'title' => 'Rawat Jalan',
				'icon' => 'icon-med-i-outpatient'
			),
			array(
				'uri' => 'rawat_inap',
				'title' => 'Rawat Inap',
				'icon' => 'icon-med-i-inpatient'
			),
			array(
				'uri' => 'ugd',
				'title' => 'UGD',
				'icon' => 'icon-med-i-emergency'
			),
			array(
				'uri' => 'odc',
				'title' => 'ODC',
				'icon' => 'fa-clock-o'
			),
			array(
				'uri' => 'ok',
				'title' => 'OK',
				'icon' => 'icon-med-i-surgery'
			),
			array(
				'uri' => 'laboratorium',
				'title' => 'Laboratorium',
				'icon' => 'icon-med-i-laboratory'
			),
			array(
				'uri' => 'radiologi',
				'title' => 'Radiologi',
				'icon' => 'icon-med-i-radiology'
			),
			array(
				'uri' => 'ct_scan',
				'title' => 'CT Scan',
				'icon' => 'icon-med-i-imaging-alternative-ct'
			),
			array(
				'uri' => 'fisioterapi',
				'title' => 'Fisioterapi',
				'icon' => 'icon-med-i-physical-therapy'
			),
			array(
				'uri' => 'audiometri',
				'title' => 'Audiometri',
				'icon' => 'icon-med-i-hearing-assistance'
			),
			array(
				'uri' => 'diagnostik_fungsional',
				'title' => 'Diagnostik Fungsional',
				'icon' => 'icon-med-i-ultrasound'
			),
			array(
				'uri' => 'cath_lab',
				'title' => 'Cath Lab',
				'icon' => 'icon-med-i-cath-lab'
			),
			array(
				'uri' => 'apotek',
				'title' => 'Apotek',
				'icon' => 'icon-med-pharmacy'
			),
			array(
				'uri' => 'farmasi',
				'title' => 'Farmasi',
				'icon' => 'icon-med-i-pharmacy'
			),
			array(
				'uri' => 'mcs',
				'title' => 'MCS',
				'icon' => 'icon-med-i-administration'
			),
			array(
				'uri' => 'pengadaan',
				'title' => 'Pengadaan Barang/Jasa',
				'icon' => 'icon-med-gift-shop'
			),
			array(
				'uri' => 'asset',
				'title' => 'Asset',
				'icon' => 'icon-med-i-gift-shop'
			),
			array(
				'uri' => 'rekam_medis',
				'title' => 'Rekam Medis',
				'icon' => 'icon-med-i-medical-records'
			),
			array(
				'uri' => 'catering',
				'title' => 'Catering',
				'icon' => 'icon-med-i-restaurant'
			),
			array(
				'uri' => 'keuangan',
				'title' => 'Keuangan',
				'icon' => 'icon-med-billing'
			),
			array(
				'uri' => 'kasir',
				'title' => 'Kasir',
				'icon' => 'icon-med-i-billing'
			),
			array(
				'uri' => 'kepegawaian',
				'title' => 'Kepegawaian',
				'icon' => 'icon-med-i-interpreter-services'
			),
			array(
				'uri' => 'laundry',
				'title' => 'Laundry',
				'icon' => 'icon-med-i-information-us'
			)
		);
		?>
		<?php foreach($url_list as $index => $item): ?>
		<?php if ($index % 2 == 0): ?>
		<div class="row">
		<?php endif; ?>
			<div class="col-md-6 mb-10">
				<div class="media">
					<div class="media-left">
						<i class="fa <?php echo $item['icon'] ?> fa-3x"></i>
					</div>

					<div class="media-body">
						<h5 class="media-heading"><?php echo $item['title'] ?></h5>
						<p><?php echo $item['icon'] ?></p>
					</div>
				</div>
			</div>
		<?php if ($index % 2 > 0): ?>
		</div>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>

<div class="panel panel-white">
	<div class="panel-heading">
		<h2 id="inser_row" class="panel-title"><i class="icon-table2 position-left"></i>DataTable - Insert Row</h2>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table id="IDTABLE_table" class="table table-bordered table-striped">
				<thead>
					<tr class="bg-slate">
						<th>Kode</th>
						<th>Nama</th>
						<th>Satuan</th>
						<th>Qty</th>
						<th></th>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td colspan="6">
							<button type="button" id="button_tambah_list" class="btn btn-xs btn-primary btn-labeled">
								<b><i class="icon-plus-circle2"></i></b>
								Tambah
							</button>
						</td>
					</tr>
				</tfoot>
			</table>

			<table style="display: none;">
				<tbody>
					<tr id="row_IDTABLE_detail_clone">
						<td>
							<input type="hidden" id="clone_IDTABLE_detail_id" />
							<input type="hidden" id="clone_barang_id" />
							<label id="clone_label_kode" class="mt-5" style="display: none;"></label>
							<input class="form-control select_kode" type="text" id="clone_disp_kode"/>
						</td>
						<td>
							<label id="clone_label_barang" class="mt-5" style="display: none;"></label>
							<div id="clone_div_barang" class="col-md-12 no-margin">
								<div class="input-group">
									<input class="form-control select_barang" type="text" id="clone_disp_barang" />
									<div class="input-group-btn">
										<button type="button" class="cari_barang btn btn-primary"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</td>
						<td>
							<label id="clone_label_satuan" class="mt-5"></label>
						</td>
						<td>
							<input type="hidden" id="clone_quantity" />
							<label id="clone_label_quantity" class="mt-5" style="display: none; text-align: right;"></label>
							<input class="quantity-row form-control" type="text" id="clone_disp_quantity" value="0" style="text-align: right;"/>
						</td>
						<td style="text-align: center; vertical-align: middle;">
							<a id="button_1"><i class="fa fa-save"></i></a> &nbsp;&nbsp;
							<a id="button_2"><i class="fa fa-times"></i></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="barang_modal" class="modal fade" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h5 class="modal-title">Daftar Barang</h5>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table id="dataTable_barang" class="table table-bordered table-striped">
						<thead>
							<tr class="bg-primary">
								<th style="width: 15%;">Kode</th>
								<th style="width: 35%;">Nama</th>
								<th style="width: 12%;">Satuan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>O-0001</td>
								<td>Gentamycin 80/ml</td>
								<td>Inj</td>
							</tr>
							<tr>
								<td>O-0002</td>
								<td>Panadol</td>
								<td>Tab</td>
							</tr>
							<tr>
								<td>A-0001</td>
								<td>Sarung Tangan</td>
								<td>Pcs</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div>
				<input type="hidden" id="counter_barang" value="0">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><b>Tutup</b></button>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('delete-modal'); ?>

<script>
var the_table;

$(window).load(function() {
	the_table = $("#dataTable_user").DataTable({
            "processing": true,
            "serverSide": false,
            "ajax": {
                "url": "<?php echo site_url('utils/style_guides/datatable_ajax'); ?>",
                "type": "POST"
            },
			"columns": [
					{ 
						"data": "nama",
						"render": function(data, type, row, meta) {
							return '<a href="#">' + data + '</a>';
						}
					},
					{ "data": "no_id" },
					{ 
						"data": "tgl.date",
						"render": function(data, type, row, meta) {
							return moment(data).isValid() ? moment(data).format('DD-MM-YYYY') : data;
						},
						"className": "text-right"
					},
					{
						"data": "jml",
						"render": function(data, type, row, meta) {
							return numeral(data).format('0,0');
						},
						"className": "text-right"
					},
					{
						"data": "saldo",
						"render": function(data, type, row, meta) {
							return numeral(data).format('$0,0.00');
						},
						"className": "text-right"
					},
					{ 
						"data": "action",
						"orderable": false,
						"render": function(data, type, row, meta) {
							return '<a href="#" title="<?php echo lang('delete') ?>" data-button="delete"><i class="fa fa-trash-o"></i></a>';
						}
					}
			]
        });
	
	the_table_barang = $("#dataTable_barang").DataTable({
            "processing": true,
        });
});

function getCounter(tr) {
	var id = tr.find('input').attr('id');
	var aId = id.split('_');
	return parseInt(aId[aId.length - 1]);
}
$(document).ready(function() {
	$("#button_tambah_list").click(function() {
		var $tr = $('#row_IDTABLE_detail_clone').clone();
		
		var counter = parseInt($("#counter").val());
		counter++;
		$("#counter").val(counter);

		$tr.attr('id', '')
		   .attr('style', '');
		
		$tr.find('#clone_IDTABLE_detail_id')
			.attr('id', 'IDTABLE_detail_id_' + counter)
			.attr('name', 'IDTABLE_detail_id[]')
			.attr('value', 'new_IDTABLE_detail_id_' + counter);
		
		$tr.find('#clone_barang_id')
			.attr('id', 'barang_id_' + counter)
			.attr('name', 'barang_id[]')
			.attr('value', 0);

		$tr.find('#clone_label_kode')
			.attr('id', 'label_kode_' + counter);
	
		$tr.find('#clone_disp_kode')
			.attr('id', 'disp_kode_' + counter)
			.attr('placeholder', 'Kode ...');
		
		$tr.find('#clone_label_barang')
			.attr('id', 'label_barang_' + counter);

		$tr.find('#clone_div_barang')
			.attr('id', 'div_barang_' + counter);
		
		$tr.find('#clone_disp_barang')
			.attr('id', 'disp_barang_' + counter)
			.attr('placeholder', 'Nama Barang ...');

		$tr.find('#clone_label_satuan')
			.attr('id', 'label_satuan_' + counter);
		
		$tr.find('#clone_quantity')
			.attr('id', 'quantity_' + counter)
			.attr('name', 'quantity[]')
			.attr('value', 1);
		
		$tr.find('#clone_label_quantity')
			.attr('id', 'label_quantity_' + counter)
			.autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
	
		$tr.find('#clone_disp_quantity')
			.attr('id', 'disp_quantity_' + counter)
			.attr('value', 1)
			.autoNumeric('init', {aSep: '.', aDec:',', aPad: false});
		
		$("#IDTABLE_table").find("tbody").append($tr);
		
		$("#button_1").attr('id', '').addClass('button_tambah_simpan').html('<i class="fa fa-save"></i>');
		$("#button_2").attr('id', '').addClass('button_tambah_batal').html('<i class="fa fa-times"></i>');
		
		$(".button_edit").prop('disabled', true);
		$(".button_hapus").prop('disabled', true);
		
		$("#button_tambah_list").prop('disabled', true);
		
		$('#simpan1').prop('disabled', true);
		$('#simpan2').prop('disabled', true);
		$('#batal1').prop('disabled', true);
		$('#batal2').prop('disabled', true);

		$tr.find('.select_kode').focus();
		
		return false;
	});

	$('#IDTABLE_table').on('click', '.button_tambah_simpan', function() {
		$tr = $(this).parent().parent();
				
		var counter = getCounter($tr);
		var kode = $tr.find('#disp_kode_' + counter).val();
		$tr.find('#label_kode_' + counter).text(kode).show();
		$tr.find('#disp_kode_' + counter).remove();

		var barang = $tr.find('#disp_barang_' + counter).val();
		$tr.find('#label_barang_' + counter).text(barang).show();
		$tr.find('#div_barang_' + counter).remove();
		
		var quantity = $tr.find('#disp_quantity_' + counter).autoNumeric('get');
		$tr.find('#quantity_' + counter).val(quantity);
		$tr.find('#label_quantity_' + counter).autoNumeric('set', quantity).show();
		$tr.find('#disp_quantity_' + counter).remove();

		$(".button_tambah_simpan").removeClass('button_tambah_simpan').addClass('button_edit');
		$(".button_edit").html('<i class="fa fa-edit"></i>');
		$(".button_tambah_batal").removeClass('button_tambah_batal').addClass('button_hapus');
		$(".button_hapus").html('<i class="fa fa-trash-o"></i>');
		
		$(".button_edit").prop('disabled', false);
		$(".button_hapus").prop('disabled', false);
		
		$("#button_tambah_list").prop('disabled', false);
		$("#button_tambah_list").focus();
		
		$('#simpan1').prop('disabled', false);
		$('#simpan2').prop('disabled', false);
		$('#batal1').prop('disabled', false);
		$('#batal2').prop('disabled', false);
		
		return false;
	});
	
	$('#IDTABLE_table').on('click', '.button_tambah_batal', function(event) {
		event.preventDefault;
		$tr = $(this).parent().parent();
		$tr.remove();

		$(".button_edit").prop('disabled', false);
		$(".button_hapus").prop('disabled', false);
		
		$("#button_tambah_list").prop('disabled', false);
		$("#button_tambah_list").focus();
		
		$('.close').show();
		$('#batal1').prop('disabled', false);
		$('#batal2').prop('disabled', false);

		tbody = $("#IDTABLE_table").find('tbody'); 
		length = tbody.children().length;

		if(length == 0) {
			$('#simpan1').prop('disabled', true);
			$('#simpan2').prop('disabled', true);
		} else {
			$('#simpan1').prop('disabled', false);
			$('#simpan2').prop('disabled', false);
		}

		return false;
	});

	$('#IDTABLE_table').on('click', '.button_edit', function() {
		$tr = $(this).parent().parent();
						
		var counter = getCounter($tr);

		var kode = $tr.find('#label_kode_' + counter).text();
		$tr.find('#label_kode_' + counter).hide();
		$tr.children('td').eq(0).append('<input id="disp_kode_' + counter + '" class="select_kode form-control" type="text" value="' + kode + '" placeholder="Kode ..." />');
		
		var barang = $tr.find('#label_barang_' + counter).text();
		$tr.find('#label_barang_' + counter).hide();
		$tr.children('td').eq(1).append('<div id="div_barang_' + counter + '" class="col-sm-12 no-margin">' +
											'<div class="input-group">' +
												'<input class="form-control select_barang" type="text" id="disp_barang_' + counter + '" value="' + barang + '" placeholder="barang ..." />' +
												'<div class="input-group-btn">' +
												'<button type="button" class="cari_barang btn btn-primary"><i class="fa fa-search"></i></button></div>' +
											'</div>' +
										'</div>');
		
		var quantity = $tr.find('#label_quantity_' + counter).autoNumeric('get');
		$tr.find('#label_quantity_' + counter).hide();
		$tr.children('td').eq(3).append('<input id="disp_quantity_' + counter + '" class="quantity-row form-control" type="text" value="' + quantity + '" style="text-align: right;" />');
		$tr.find('#disp_quantity_' + counter)
			.autoNumeric('init', {aSep: '.', aDec:',', aPad: false})
			.focus()
			.select();
		
		$tr.find(".button_edit").removeClass('button_edit').addClass('button_edit_simpan');
		$tr.find(".button_edit_simpan").html('<i class="fa fa-save"></i>');
		$tr.find(".button_hapus").removeClass('button_hapus').addClass('button_edit_batal');
		$tr.find(".button_edit_batal").html('<i class="fa fa-times"></i>');
		
		$(".button_edit").prop('disabled', true);
		$(".button_hapus").prop('disabled', true);
		
		$("#button_tambah_list").prop('disabled', true);
		
		$('#simpan1').prop('disabled', true);
		$('#simpan2').prop('disabled', true);
		$('#batal1').prop('disabled', true);
		$('#batal2').prop('disabled', true);

		return false;
	});
	
	$('#IDTABLE_table').on('click', '.button_hapus', function(event) {
		event.preventDefault;
		$tr = $(this).parent().parent();

		var counter = getCounter($tr);
		$tr.remove();

        $("#button_tambah_list").focus();

        tbody = $("#IDTABLE_table").find('tbody'); 
		length = tbody.children().length;

		if(length == 0) {
			$('#simpan1').prop('disabled', true);
			$('#simpan2').prop('disabled', true);
		} else {
			$('#simpan1').prop('disabled', false);
			$('#simpan2').prop('disabled', false);
		}
		return false;
	});
	
	$('#IDTABLE_table').on('click', '.button_edit_simpan', function(event) {
		$tr = $(this).parent().parent();
				
		var counter = getCounter($tr);

		var kode = $tr.find('#disp_kode_' + counter).val();
		$tr.find('#label_kode_' + counter).text(kode).show();
		$tr.find('#disp_kode_' + counter).remove();
		
		var barang = $tr.find('#disp_barang_' + counter).val();
		$tr.find('#div_barang_' + counter).remove();
		$tr.find('#label_barang_' + counter).text(barang).show();
		
		var quantity = $tr.find('#disp_quantity_' + counter).autoNumeric('get');
		$tr.find('#quantity_' + counter).val(quantity);
		$tr.find('#label_quantity_' + counter).autoNumeric('set', quantity).show();
		$tr.find('#disp_quantity_' + counter).remove();

		$(".button_edit_simpan").removeClass('button_edit_simpan').addClass('button_edit');
		$(".button_edit").html('<i class="fa fa-edit"></i>');
		$(".button_edit_batal").removeClass('button_edit_batal').addClass('button_hapus');
		$(".button_hapus").html('<i class="fa fa-trash-o"></i>');
		
		$(".button_edit").prop('disabled', false);
		$(".button_hapus").prop('disabled', false);
		
		$("#button_tambah_list").prop('disabled', false);
		$("#button_tambah_list").focus();
		
		$('#simpan1').prop('disabled', false);
		$('#simpan2').prop('disabled', false);
		$('#batal1').prop('disabled', false);
		$('#batal2').prop('disabled', false);
		
		return false;
	});
	
	$('#IDTABLE_table').on('click', '.button_edit_batal', function() {
	
		$tr = $(this).parent().parent();
				
		var counter = getCounter($tr);

		$tr.find('#disp_kode_' + counter).remove();
		$tr.find('#label_kode_' + counter).show();

		$tr.find('#div_barang_' + counter).remove();
		$tr.find('#label_barang_' + counter).show();
		
		$tr.find('#disp_quantity_' + counter).remove();
		$tr.find('#label_quantity_' + counter).show();
		
		$(".button_edit_simpan").removeClass('button_edit_simpan').addClass('button_edit');
		$(".button_edit").html('<i class="fa fa-edit"></i>');
		$(".button_edit_batal").removeClass('button_edit_batal').addClass('button_hapus');
		$(".button_hapus").html('<i class="fa fa-trash-o"></i>');
		
		$(".button_edit").prop('disabled', false);
		$(".button_hapus").prop('disabled', false);
		
		$("#button_tambah_list").prop('disabled', false);
		$("#button_tambah_list").focus();
		
		$('#simpan1').prop('disabled', false);
		$('#simpan2').prop('disabled', false);
		$('#batal1').prop('disabled', false);
		$('#batal2').prop('disabled', false);

		return false;
	});

	$("#IDTABLE_table").on("click", ".cari_barang", function() {
		$tr = $(this).parent().parent();
		var counter = getCounter($tr);
		$('#counter_barang').val(counter);
		$("#barang_modal").modal('show');
	});
});
</script>