<style>
.thumb > img {
    min-height: 256;
    border: 1px solid #e9e9e9;
}
</style>
<div class="row">
    <?php foreach ($list_app as $row): ?>
        <div class="box-layar col-lg-3 col-sm-6">
            <div class="thumbnail">
                <div class="thumb">
                    <?php
                        $image_src = base_url('assets/img/no_image.png');
                        if(!is_null($row->image) && $row->image != "") 
                            $image_src = base_url('assets/menu/'.$row->image);
                    ?>
                    <img src="<?php echo $image_src; ?>" alt="">
                </div>
                <div class="caption">
                    <h6 class="no-margin-top text-semibold text-center">
                        <a href="<?php echo site_url('app/app/show/'.$row->uid); ?>">
                             <?php echo ucwords($row->name); ?>
                         </a>
                         <!-- <a href="<?php echo $row->url."?data=".$data_user; ?>" target="_blank">
                            <?php echo ucwords($row->name); ?>
                                                 </a>  -->
                    </h6>
                    <?php echo !is_null($row->description) ? $row->description : ''; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>