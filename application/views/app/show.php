<style type="text/css">
	.loader {
	    position:absolute;
	    left:45%;
	    top:35%;
	    border-radius:20px;
	    padding:25px;
	    border:1px solid #777777;
	    background:#ffffff;
	    box-shadow:0px 0px 10px #777777;
	}
</style>
<img class="loader" src="<?php echo assets_url('img/spinner.gif')?>" />
<div class="iframe-content"></div>

<script type="text/javascript">
	var iframeHeight = $(window).height() - 20;
	$('.iframe-content').html('<iframe id="iframe" src="<?php echo $app_url; ?>" style="width: 100%; height: ' + iframeHeight + 'px; border: 1px solid #e5e5e5;"></iframe>');

	$(document).ready(function() {
		$('#iframe').on('load', function() {
			$('.loader').hide();
		});
	});
</script>