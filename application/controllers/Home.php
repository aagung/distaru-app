<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Home extends Admin_Controller {
	
	protected $page_title = '<i class="fa fa-dashboard"></i> Home';
	protected $page_icons = '';

	public function index()
	{
		redirect('app/app');
	}
}
