<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends Admin_Controller {
	protected $data_user;
	function __construct() {
		parent::__construct();

		$user = $this->db->query("SELECT no_ktp, CONCAT(first_name, ' ', last_name) name, email, password FROM auth_users WHERE id = ".$this->auth->userid())->row();
		$user->password = base64_decode($this->session->userdata('x-token'));
		$this->data_user = base64_encode(json_encode($user));
		$this->load->vars(array(
			'data_user' => $this->data_user,
		));
	}

	public function index()
	{
		$this->data['list_app'] = $this->db->query("SELECT * FROM m_app WHERE status = 1")->result();
		$this->template->set_layout('admin')
			->build('app/index', $this->data);
	}

	public function show($uid = '')
	{
		$app = $this->db->query("SELECT url FROM m_app WHERE uid = '{$uid}'")->row();
		$this->data['app_url'] = $app->url.'?data='.$this->data_user;
		$this->data['back_button'] = site_url('app/app');
		$this->data['list_app'] = $this->db->query("SELECT * FROM m_app WHERE status = 1")->result();
		$this->template->set_layout('without_navbar')
			->build('app/show', $this->data);
	}
}
