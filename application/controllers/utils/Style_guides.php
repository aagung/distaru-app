<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Display Log Files.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Style_guides extends Admin_Controller
{
	protected $page_title = '<i class="fa fa-wrench"></i>Style Guides';
	protected $faker;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->faker = Faker\Factory::create('id_ID');
		$this->faker->seed(1645985237);
	}
	
	public function index() 
	{
		$this->template
				->set_js('plugins/tables/datatables/datatables.min', FALSE)
				->set_js('plugins/ui/prism.min', FALSE)
				->set_js('plugins/autoNumeric/autoNumeric-min', TRUE)
				->build('utils/style_guides/index');
	}
	
	public function datatable_ajax()
	{
		$rows = array();
		for ($i = 0; $i < 500; $i++) {
			$rows[] = array(
				'nama' => $this->faker->name,
				'no_id' => $this->faker->numerify('########'),
				'tgl' => $this->faker->dateTimeBetween('-40 years', '-20 years'),
				'jml' => $this->faker->numberBetween(1000, 10000),
				'saldo' => $this->faker->numberBetween(1000000, 1000000000)
			);
		}
		echo json_encode(array('data' => $rows));
	}
}