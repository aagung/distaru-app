<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Jenssegers\Date\Date;

if (!function_exists('datetostr'))
{
	function datetostr($timestamp = FALSE, $format = 'd/m/Y')
	{
		$CI =& get_instance();
		
		Date::setLocale($CI->load->get_var('lang'));
		if ($timestamp === FALSE)
			$date = Date::create();
		elseif ($timestamp == '0000-00-00')
			return '';
		else
			$date = Date::parse($timestamp);
		
		return $date->format($format);
	}
}

if (!function_exists('strtodate')) {
	function strtodate($datestr = FALSE, $format = 'd/m/Y')
	{
		$CI =& get_instance();
		
		Date::setLocale($CI->load->get_var('lang'));
		if ($datestr === FALSE)
			$date = Date::create();
		elseif (empty($datestr) || $datestr == '00-00-0000')
			return '';
		else
			$date = Date::createFromFormat ($format, $datestr);
		
		return $date->format('Y-m-d H:i:s');
	}
}

if (!function_exists('diffforhumans')) {
	function diffforhumans($datestr = FALSE, $format = 'Y-m-d H:i:s')
	{
		$CI =& get_instance();
		
		Date::setLocale($CI->load->get_var('lang'));
		if ($datestr === FALSE)
			$date = Date::create();
		elseif (empty($datestr) || $datestr == '00-00-0000')
			return '';
		else
			$date = Date::createFromFormat ($format, $datestr);
		
		return $date->diffForHumans();
	}
}

