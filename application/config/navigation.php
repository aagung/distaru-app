<?php

/**
 * Main Navigation.
 * Primarily being used in views/layouts/admin.php
 * 
 */
$config['navigation'] = array(
	'app' => array(
		'uri' => 'app/app',
		'title' => 'Application',
		'icon' => 'fa fa-gear'
	),
	'user-management' => array(
		'uri' => 'auth/user',
		'title' => 'User Management',
		'icon' => 'fa fa-user'
	),
	'acl' => array(
		'title' => 'ACL',
		'icon' => 'fa fa-unlock-alt',
		'children' => array(
			'rules' => array(
				'uri' => 'acl/rule',
				'title' => 'Rules'
			),
			'roles' => array(
				'uri' => 'acl/role',
				'title' => 'Roles'
			),
			'resources' => array(
				'uri' => 'acl/resource',
				'title' => 'Resources'
			)
		)
	),
	'utils' => array(
		'title' => 'Utils',
		'icon' => 'fa fa-wrench',
		'children' => array(
			'style_guides' => array(
				'uri' => 'utils/style_guides',
				'title' => 'Style Guides'
			),
			'system_logs' => array(
				'uri' => 'utils/logs/system',
				'title' => 'System Logs'
			),
			/*'deploy_logs' => array(
				'uri' => 'utils/logs/deploy',
				'title' => 'Deploy Logs'
			),*/
			'info' => array(
				'uri' => 'utils/info',
				'title' => 'Info'
			)
		)
	),
);