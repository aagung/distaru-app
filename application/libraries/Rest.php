<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

use GuzzleHttp\Client;

/**
 * Guzzle Handler.
 * 
 * @package CodeIgniter
 * @subpackage Libraries
 * @category Libraries
 * @author Rizki Fauzi
 */
class Rest
{

	var $CI, $base_url, $api_key;

	function __construct()
	{
		// Get the instance
		$this->CI = & get_instance();
		$this->base_url = $this->CI->config->item('api_base_url');
		$this->api_key = $this->CI->config->item('api_token');
	}

	function get($url, $data = array())
	{
		$client = new Client();
		try {
			$response = $client->request('GET', $this->base_url . $url . $this->createQueryString($data), [
				'headers' => ['token' => $this->api_key]
			]);
			//guzzle response
			/* echo $response->getStatusCode(); // 200
			 *  echo $response->getReasonPhrase(); // OK
			 *  echo $response->getProtocolVersion(); // 1.1
			 */
			return $response->getBody();
		} catch (GuzzleHttp\Exception\BadResponseException $e) {
			$response = $e->getResponse();
			$responseBodyAsString = $response->getBody()->getContents();
			return $responseBodyAsString;
		}
	}

	function post($url, $data = array())
	{
		$jsonData = json_encode($data);
		$client = new Client();
		try {
			$response = $client->request('POST', $this->base_url . $url, [
				'headers' => ['token' => $this->api_key],
				'body' => $jsonData
			]);
			//guzzle response
			/* echo $response->getStatusCode(); // 200
			 *  echo $response->getReasonPhrase(); // OK
			 *  echo $response->getProtocolVersion(); // 1.1
			 */
			return $response->getBody();
		} catch (GuzzleHttp\Exception\BadResponseException $e) {
			$response = $e->getResponse();
			$responseBodyAsString = $response->getBody()->getContents();
			return $responseBodyAsString;
		}
	}

	function createQueryString($data = array())
	{
		$queryString = '';
		if (!empty($data)) {
			$queryString = http_build_query($data);
		}

		$cekQueryString = !empty($queryString) ? '?' . $queryString : '';
		return $cekQueryString;
	}

	function update($url, $data = array())
	{
		$jsonData = json_encode($data);
		$client = new Client();
		try {
			$response = $client->request('POST', $this->base_url . $url, [
				'headers' => ['token' => $this->api_key],
				'body' => $jsonData
			]);
			//guzzle response
			/* echo $response->getStatusCode(); // 200
			 *  echo $response->getReasonPhrase(); // OK
			 *  echo $response->getProtocolVersion(); // 1.1
			 */
			return $response->getBody();
		} catch (GuzzleHttp\Exception\BadResponseException $e) {
			$response = $e->getResponse();
			$responseBodyAsString = $response->getBody()->getContents();
			return $responseBodyAsString;
		}
	}

	function delete($url, $data = array())
	{
		$client = new Client();
		try {
			$response = $client->request('DELETE', $this->base_url . $url . $this->createQueryString($data), [
				'headers' => ['token' => $this->api_key]
			]);
			//guzzle response
			/* echo $response->getStatusCode(); // 200
			 *  echo $response->getReasonPhrase(); // OK
			 *  echo $response->getProtocolVersion(); // 1.1
			 */
			return $response->getBody();
		} catch (GuzzleHttp\Exception\BadResponseException $e) {
			$response = $e->getResponse();
			$responseBodyAsString = $response->getBody()->getContents();
			return $responseBodyAsString;
		}
	}
}
